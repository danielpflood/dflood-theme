<!DOCTYPE HTML>
<html lang=en>

<head profile="http://gmpg.org/xfn/11">
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>
	; charset=
	<?php bloginfo('charset'); ?>
	" />
	<title><?php if ( is_single() ) { ?>
		<?php } ?>
		<?php wp_title(); ?>
		:
		<?php bloginfo('name'); ?></title>

	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>
	" type="text/css" media="screen, print" />
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?>
	RSS Feed" href="
	<?php bloginfo('rss2_url'); ?>
	" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>
	" />
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" ></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js" ></script>

	<?php wp_head(); ?>
</head>
<body>

	<div id="container">