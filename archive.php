<?php get_header(); ?>

	<div id="content">
<h2><a href="<?php echo home_url(); ?>"><?php echo get_bloginfo('name'); ?></a>.<a href="<?php get_the_category_list(); ?>"><?php echo single_cat_title(); ?></a></h2>

<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>
			<div class="post" id="post-<?php the_ID(); ?>">
				<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
			</div>
		<?php endwhile; ?>
	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>

	<?php endif; ?>
</div>

<?php get_footer(); ?>
