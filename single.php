<?php get_header(); ?>
	<div id="content">
		<div class="post" id="post-<?php the_ID(); ?>">
			<h2><a href="<?php echo home_url(); ?>"><?php echo get_bloginfo('name'); ?></a>.<?php the_category('.'); ?>.<b><?php the_title(); ?></b></h2>
			<div class="entry">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<?php the_content('<p class="serif">Read the rest of this entry &raquo;</p>'); ?>

				<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
			</div>
		</div><br /><br />
			<?php //comments_template(); ?>
	<?php endwhile; else: ?>
		<p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>
	</div>
<?php get_footer(); ?>