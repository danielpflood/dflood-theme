<?php get_header(); ?>
	<div id="content">
		<div class="post" id="post-<?php the_ID(); ?>">
			<h2><a href="<?php echo home_url(); ?>"><?php echo get_bloginfo('name'); ?></a> : <b><?php the_title(); ?></b></h2>
			<div class="entry">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
				<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
			</div>
		</div>
				<?php //comments_template(); ?>
		<?php endwhile; endif; ?>
	<small><?php edit_post_link('Edit', '<p>', '</p>'); ?></small>
	</div>
<?php get_footer(); ?>